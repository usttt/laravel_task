@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <a class="btn btn-info mb-2" href="{{url('workers/create')}}">Создать</a>

        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Отчество</th>
                    <th>Пол</th>
                    <th>Заработная плата</th>
                    <th>Названия отделов</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach($workers as $worker)
                    <tr>
                        <td>
                            {{$worker->name}}
                        </td>
                        <td>
                            {{$worker->surname}}
                        </td>
                        <td>
                            {{$worker->lastname}}
                        </td>
                        <td>
                            {{$worker->sex == 'man' ? 'Мужской' : 'Женский'}}
                        </td>
                        <td>
                            {{$worker->salary}}
                        </td>
                        <td>
                            {{$worker->departments->pluck('name')->implode(', ')}}
                        </td>
                        <td>

                            <a href="{{url('workers/'.$worker->id.'/edit')}}" class="btn btn-warning">Изменить</a>

                            <a href="{{url('worker/delete/'.$worker->id)}}" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

@endsection
