@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <form id="form">
            <div class="form-group">
                <label for="name">Имя <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name">
            </div>

            <div class="form-group">
                <label for="name">Фамилия <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="surname">
            </div>

            <div class="form-group">
                <label for="name">Отчество</label>
                <input type="text" class="form-control" name="lastname">
            </div>

            <div class="form-group">
                <label for="name">Пол</label>
                <select name="sex" class="custom-select">
                    <option value="man">Мужской</option>
                    <option value="woman">Женский</option>
                </select>
            </div>

            <div class="form-group">
                <label for="name">Зар плата</label>
                <input type="number" class="form-control" name="salary">
            </div>

            <div class="form-group">
                <label for="name">Отдел</label>
                <select name="departments" class="custom-select" id="" multiple>
                    @foreach($departments as $department)
                        <option value="{{$department->id}}">{{$department->name}}</option>
                    @endforeach
                </select>
            </div>

            <button type="button" class="btn btn-info" id="store">Создать</button>

        </form>

        <div id="alert" class="mt-2">

        </div>

    </div>

    <script>
        $( document ).ready(function() {

            var csrf = $('meta[name="csrf-token"]').attr('content');
            $('#store').click(function () {

                $("button").attr("disabled", true);

                var _token = $('meta[name="csrf-token"]').attr('content');

                var name = $("input[name=name]").val();

                var surname = $("input[name=surname]").val();

                var lastname = $("input[name=lastname]").val();

                var sex = $("select[name=sex]").val();

                var salary = $("input[name=salary]").val();

                var departments = $("select[name=departments]").val();

                var alert = $("#alert");

                $.ajax({
                    url: '{{url('workers')}}',
                    data:{
                        name:name,
                        surname:surname,
                        lastname:lastname,
                        sex:sex,
                        salary:salary,
                        departments:departments,
                        _token: _token
                    },

                    type: 'POST',

                    success: function(data){

                        alert.empty();

                        if(data.success)
                        {
                            alert.append('<p class="text-success"> '+ data.message+'</p>');
                        }

                        else{

                            $.each(data.message,function(index,value){
                                alert.append('<p class="text-danger"> '+ value +'</p>')
                            });

                        }

                        $("button").attr("disabled", false);
                    }
                });
            });
        });

    </script>
@endsection
