@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>

                    </th>
                    @foreach($departments as $department)
                        <th>
                            {{$department->name}}
                        </th>
                    @endforeach
                </tr>
            </thead>

            <tbody>

                    @foreach($workers as $worker)
                        <tr>
                            <td>
                                {{$worker->name .' '. $worker->surname  }}
                            </td>

                            @for($i = 0; $i<$departments->count(); $i++)
                                <td>

                                    {!!  $departments[$i]->workers->where('id', $worker->id)->first() != null ? "<p>&#10003</p>" : ''!!}

                                </td>
                            @endfor
                        </tr>
                    @endforeach

            </tbody>
        </table>

    </div>

@endsection
