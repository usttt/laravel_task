@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <a class="btn btn-info mb-2" href="{{url('departments/create')}}">Создать</a>

        @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{session()->get('error')}}
        </div>
        @endif

        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Название отдела</th>
                    <th>Количество сотрудников отдела</th>
                    <th>Максимальная заработная плата среди сотрудников отдела</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach($departments as $department)
                    <tr>
                        <td>
                            {{$department->name}}
                        </td>
                        <td>
                            {{$department->workers->count()}}
                        </td>
                        <td>
                            {{$department->workers->max('salary')}}
                        </td>
                        <td>

                            <a href="{{url('departments/'.$department->id.'/edit')}}" class="btn btn-warning">Изменить</a>

                            <a href="{{url('department/delete/'.$department->id)}}" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

@endsection
