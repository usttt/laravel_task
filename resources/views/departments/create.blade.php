@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <form id="form">
            <div class="form-group">
                <label for="name">Название отдела</label>
                <input type="text" class="form-control" name="name">
            </div>

            <button type="button" class="btn btn-info" id="store">Создать</button>

        </form>

        <div id="alert" class="mt-2">

        </div>

    </div>

    <script>
        $( document ).ready(function() {

            var csrf = $('meta[name="csrf-token"]').attr('content');
            $('#store').click(function () {

                $("button").attr("disabled", true);

                var _token = $('meta[name="csrf-token"]').attr('content');

                var name = $("input[name=name]").val();

                var alert = $("#alert");

                $.ajax({
                    url: '{{url('departments')}}',
                    data:{
                        name:name,
                        _token: _token
                    },

                    type: 'POST',

                    success: function(data){

                        alert.empty();

                        if(data.success)
                        {
                            alert.append('<p class="text-success"> '+ data.message+'</p>');
                        }

                        else{

                            $.each(data.message,function(index,value){
                                alert.append('<p class="text-danger"> '+ value +'</p>')
                            });

                        }

                        $("button").attr("disabled", false);

                    }
                });
            });
        });

    </script>
@endsection
