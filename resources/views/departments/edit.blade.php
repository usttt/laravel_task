@extends('layouts.main')

@section('content')

    <div class="container m-4">

        <form id="form">
            <div class="form-group">
                <label for="name">Название отдела</label>
                <input type="text" value="{{$data->name}}" class="form-control" name="name">
            </div>

            <button type="button" class="btn btn-info" id="edit">Изменить</button>

        </form>

        <div id="alert" class="mt-2">

        </div>

    </div>

    <script>
        $( document ).ready(function() {

            var csrf = $('meta[name="csrf-token"]').attr('content');
            $('#edit').click(function () {
                /*$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });*/

                $("button").attr("disabled", true);

                var _token = $('meta[name="csrf-token"]').attr('content');

                var name = $("input[name=name]").val();

                var alert = $("#alert");

                $.ajax({
                    url: '{{url('departments/'.$data->id)}}',
                    data:{
                        name:name,
                        _token: _token
                    },

                    type: 'PUT',

                    success: function(data){

                        alert.empty();

                        if(data.success)
                        {
                            alert.append('<p class="text-success"> '+ data.message+'</p>');
                        }

                        else{

                            $.each(data.message,function(index,value){
                                alert.append('<p class="text-danger"> '+ value +'</p>')
                            });

                        }

                        $("button").attr("disabled", false);
                    }
                });
            });
        });

    </script>
@endsection
