<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'GridController@index');

Route::resource('departments', 'DepartmentController');

Route::get('department/delete/{id}', 'DepartmentController@destroy');

Route::resource('workers', 'WorkersController');

Route::get('worker/delete/{id}', 'WorkersController@destroy');

