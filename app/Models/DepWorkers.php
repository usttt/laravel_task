<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepWorkers extends Model
{
    protected $fillable = ['department_id', 'workers_id'];
}
