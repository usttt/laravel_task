<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workers extends Model
{
    protected $table = 'workers';

    protected $fillable = ['name', 'surname', 'lastname', 'salary', 'sex'];

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'dep_workers');
    }
}
