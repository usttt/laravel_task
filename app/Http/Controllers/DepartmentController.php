<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $departments = Department::orderBy('id', 'DESC')->get();

        return view('departments.index', compact('departments'));
    }

    public function create()
    {
        return view('departments.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if($validator->fails())
        {
            return response()->json([
                'success'   => false,
                'message'   => $validator->errors(),
            ]);

        }

        $name = $request->input('name');

        $department = new Department();

        $department->name = $name;

        $department->save();

        return response()->json([
            'success'   => true,
            'message'   => ['Успешно'],
        ]);
    }

    public function edit($id)
    {
        $data = Department::find($id);

        return view('departments.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if($validator->fails())
        {
            return response()->json([
                'success'   => false,
                'message'   => $validator->errors(),
            ]);

        }

        $name = $request->input('name');

        $department = Department::find($id);

        $department->name = $name;

        $department->save();

        return response()->json([
            'success'   => true,
            'message'   => ['Успешно'],
        ]);
    }

    public function destroy($id)
    {
        $department =  Department::find($id);

        if($department->workers->count())
        {
            return back()->with('error', 'Невозможно удалить запись!');
        }

        $department->delete();

        return  back()->with('success', 'Успешно удалено!');
    }
}
