<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Workers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkersController extends Controller
{
    public function index(Request $request)
    {
        $workers = Workers::orderBy('id', 'DESC')->get();

        return view('workers.index', compact('workers'));
    }

    public function create()
    {
        $departments = Department::all();

        return view('workers.create', compact('departments'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'surname' => ['required'],
            'departments' => ['required', 'array'],
            'salary' => ['numeric', 'nullable'],
        ]);

        if($validator->fails())
        {
            return response()->json([
                'success'   => false,
                'message'   => $validator->errors(),
            ]);

        }

        $name = $request->input('name');

        $surname = $request->input('surname');

        $lastname = $request->input('lastname');

        $sex = $request->input('sex');

        $salary = $request->input('salary');

        $departments = $request->input('departments');

        $workers = new Workers();

        $workers->name = $name;
        $workers->surname = $surname;
        $workers->lastname = $lastname;
        $workers->sex = $sex;
        $workers->salary = $salary;
        $workers->save();

        $worker = Workers::find($workers->id);

        $worker->departments()->attach($departments);

        $worker->save();

        return response()->json([
            'success'   => true,
            'message'   => ['Успешно'],
        ]);
    }

    public function edit($id)
    {
        $data = Workers::find($id);

        $departments = Department::all();

        return view('workers.edit', compact('data', 'departments'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'surname' => ['required'],
            'departments' => ['required', 'array'],
            'salary' => ['numeric', 'nullable'],
        ]);

        if($validator->fails())
        {
            return response()->json([
                'success'   => false,
                'message'   => $validator->errors(),
            ]);

        }

        $name = $request->input('name');

        $surname = $request->input('surname');

        $lastname = $request->input('lastname');

        $sex = $request->input('sex');

        $salary = $request->input('salary');

        $departments = $request->input('departments');

        $workers = Workers::find($id);

        $workers->name = $name;
        $workers->surname = $surname;
        $workers->lastname = $lastname;
        $workers->sex = $sex;
        $workers->salary = $salary;
        $workers->save();

        $worker = Workers::find($id);

        $worker->departments()->sync($departments);

        $worker->save();

        return response()->json([
            'success'   => true,
            'message'   => ['Успешно'],
        ]);
    }

    public function destroy($id)
    {
        Workers::find($id)->delete();

        return  back()->with('success', 'Успешно удалено!');
    }
}
