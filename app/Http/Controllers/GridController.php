<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Workers;
use Illuminate\Http\Request;

class GridController extends Controller
{
    public function index()
    {
        $departments = Department::all();

        $workers = Workers::all();

        return view('home.home', compact('departments', 'workers'));
    }
}
